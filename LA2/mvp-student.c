#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>
#include <stdint.h>
#include "myheader.h"
#define BILLION 1000000000L

// Computes matrix-vector product from LA1
double* mvp(double** mat, double* vec, int n){

	double* out = (double*)malloc(n*sizeof(double));

	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			out[i] += mat[i][j] * vec[j];
		}
	}

	return out;
}

// Prints LA1 output
void printMatVec(double** mat, double* vec, double* out, int n){
	
	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			printf("%.2f ", mat[i][j]);

			if(j == n-1 && i == 2)
				printf("  %.2f = %.2f", vec[i], out[i]);
			else if(j == n-1)
				printf("  %.2f   %.2f", vec[i], out[i]);
		}
		printf("\n");
	}
}

// Deallocates an N by N matrix
void freeMat(double** mat, int n){
        
	for(int i = 0; i < n; i++){
                free(mat[i]);
        }
        free(mat);
}

// Allocates a matrix of size N by N
double** allocMat(int n){
        
	double** mat = (double**)malloc(n*sizeof(double*));
        for(int i = 0; i < n; i++){
                mat[i] = (double*)malloc(n*sizeof(double));
        }
	
	return mat;
}

// Fills a N by N matrix with 2's on the diagonal, 1's on the off diagonal, and 0's elsewhere
void assignMat(double** mat, int n){
	
        for(int i = 0; i < n; i++)
                for(int j = 0; j < n; j++){
                        if(i == j)
                                mat[i][j] = 2.0;
                        else if(i - j == 1 || i - j == -1)
                                mat[i][j] = 1.0;
                        else
                                mat[i][j] = 0.0;
        }
}

// Allocates a vector of size N
double* allocVec(int n){
	
	double* vec = (double*)malloc(n*sizeof(double));
	return vec;
}

// Fills a vector of size N with all ones
void assignVec(double* vec, int n){
	
        for(int i = 0; i < n; i++){
                vec[i] = 1.0;
        }
}

// Allocates a matrix of size N by M
double** allocMatNxM(int n, int m){

        double** mat = (double**)malloc(n*sizeof(double*));
        for(int i = 0; i < n; i++){
                mat[i] = (double*)malloc(m*sizeof(double));
        }

        return mat;
}

// Fills a N by M matrix with 2's on the diagonal, 1's on the off diagonal, and 0's elsewhere
void fillMatNxM(double** mat, int n, int m){
	
        for(int i = 0; i < n; i++)
                for(int j = 0; j < m; j++){
                        if(i == j)
                                mat[i][j] = 2.0;
                        else if(i - j == 1 || i - j == -1)
                                mat[i][j] = 1.0;
                        else
                                mat[i][j] = 0.0;
        }
}

// Fills an N by M matrix with all ones
void fillMatNxMOnes(double** mat, int n, int m){

	for(int i = 0; i < n; i++){
		for(int j = 0; j < m; j++){
			mat[i][j] = 1.0;
		}
	}
}

// Displays contents of an N by M matrix
void printMatNxM(double** mat, int n, int m){

	for(int i = 0; i < n; i++){
		for(int j = 0; j < m; j++){
			printf("%f ", mat[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

// Performs dot product for two vectors
void dotRowCol(double** row, double** col, int rownum, int colnum, int vecsize, double** out){

	double result = 0;
	for(int i = 0; i < vecsize; i++){
		result += row[rownum][i] * col[i][colnum];
	}

	out[rownum][colnum] = result;
}

int main(int argc, char *argv[]){

	if(argc < 5){
		printf("\nUSAGE: ./a.out matrix1_Dim1 matrix1_Dim2 matrix2_Dim1 matrix2_Dim2\n\n");
		exit(1);
	}
	if(atoi(argv[2]) != atoi(argv[3])){
		printf("\nInner dimensions of matrices do no match. Cannot multiply.\n\n");
		exit(2);
	}

	int mat1_dim1 = atoi(argv[1]);
	int mat1_dim2 = atoi(argv[2]);
	int mat2_dim1 = atoi(argv[3]);
	int mat2_dim2 = atoi(argv[4]);

	if(mat1_dim1 <= 0 || mat1_dim2 <= 0 || mat2_dim1 <= 0 || mat2_dim2 <= 0){
		printf("\nMatrix dimensions must be greater than or equal to 1.\n\n");
	}

	// Alloc
	double** mat1 = allocMatNxM(mat1_dim1, mat1_dim2);
	double** mat2 = allocMatNxM(mat2_dim1, mat2_dim2);
	double** out = allocMatNxM(mat1_dim1, mat2_dim2);
	
	// Fill (no need to fill out because everything will get overwritten)
	fillMatNxM(mat1, mat1_dim1, mat1_dim2);
	fillMatNxMOnes(mat2, mat2_dim1, mat2_dim2);

	// Friendly timing measures that compile with or without -fopenmp
	uint64_t diff;
	struct timespec start, end;
	
	clock_gettime(CLOCK_MONOTONIC, &start);

	#pragma omp parallel for num_threads(12) schedule(dynamic)
	for(int i = 0; i < mat1_dim1; i++){
		for(int j = 0; j < mat2_dim2; j++){
			dotRowCol(mat1, mat2, i, j, mat1_dim2, out);
		}
	}

	clock_gettime(CLOCK_MONOTONIC, &end);

	diff = BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec;
	printf("\nelapsed time = %llu nanoseconds\n\n", (long long unsigned int) diff);

	// Uncomment following block to display the results

	//printf("Matrix 1:\n");
	//printMatNxM(mat1, mat1_dim1, mat1_dim2);
	//printf("Matrix 2:\n");
	//printMatNxM(mat2, mat2_dim1, mat2_dim2);
	//printf("Result of multiplication:\n");
	//printMatNxM(out, mat1_dim1, mat2_dim2);
	
	// Repay debt to heap
	freeMat(mat1, mat1_dim1);
	freeMat(mat2, mat2_dim1);
	freeMat(out, mat1_dim1);
		
    	return 0;
}

 
