#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>
#include <stdint.h>
#define BILLION 1000000000L
#define KERNEL_LEN 3

extern int* imageToMat(char* name,int* dims);
extern void matToImage(char* name, int* mat, int* dims);

void convolutionFlatKernel(int** paddedMat, int* mat, int height, int width) {

	int flat_kernel[] = {1, 0, -1, 2, 0, -2, 1, 0, -1};

        int kernelmid = KERNEL_LEN/2;

        #pragma omp parallel num_threads(12)
        {
          #pragma omp for schedule(dynamic)
          for(int i = 1; i < height-1; i++) {
                  for(int j = 1; j < width-1; j++) {
                          int sum = 0;
                          for(int k = -kernelmid; k < kernelmid+1; k++) {
                                  for(int l = -kernelmid; l < kernelmid+1; l++) {
                                          sum += flat_kernel[(kernelmid+k)*3 + (kernelmid+l)] * paddedMat[i+k][j+l];
                                  }
                          }
                          mat[((i-1)*(width-2))+(j-1)] = sum;
                  }
          }
        }
}

void convolution(int** kernel, int** paddedMat, int* mat, int height, int width) {

	int kernelmid = KERNEL_LEN/2;
	
	#pragma omp parallel num_threads(12)
	{
	  #pragma omp for schedule(dynamic)
	  for(int i = 1; i < height-1; i++) {
	  	  for(int j = 1; j < width-1; j++) {
			  int sum = 0;
			  for(int k = -kernelmid; k < kernelmid+1; k++) {
				  for(int l = -kernelmid; l < kernelmid+1; l++) {
					  sum += kernel[kernelmid+k][kernelmid+l] * paddedMat[i+k][j+l];
				  }
			  }
			  mat[((i-1)*(width-2))+(j-1)] = sum;
		  }
	  }
	}
}

int main(int argc, char** argv){

    int* mat;
    char* name="image.jpg";
    int* dims;
    dims=(int*) malloc(sizeof(*dims)*2);

    mat=imageToMat(name,dims);

    // Alloc mat with pad
    int** paddedMat = (int**)malloc((dims[0]+2)*sizeof(int*));
    for(int i = 0; i < dims[0]+2; i++)
    	paddedMat[i] = (int*)malloc((dims[1]+2)*sizeof(int));
    
    // Fill padded mat
    for(int i = 0; i < dims[0]+2; i++)
    	for(int j = 0; j < dims[1]+2; j++)
		paddedMat[i][j] = 1;
    for(int i = 0; i < dims[0]; i++)
    	for(int j = 0; j < dims[1]; j++)
		paddedMat[i+1][j+1] = mat[i*dims[1]+j];
    
    // Alloc kernel
    int** kernel = (int**)malloc(KERNEL_LEN*sizeof(int*));
    for(int i = 0; i < KERNEL_LEN; i++)
    	kernel[i] = (int*)malloc(KERNEL_LEN*sizeof(int));

    // Fill kernel (sobel filter)
    /*
       [[1, 0, -1]
	[2, 0, -2]
	[1, 0, -1]]
    */
    
    for(int i = 0 ; i < KERNEL_LEN; i++)
    	for(int j = 0; j < KERNEL_LEN; j++)
		kernel[i][j] = 0;
    kernel[0][0] =  1;
    kernel[1][0] =  2;
    kernel[2][0] =  1;
    kernel[0][2] = -1;
    kernel[1][2] = -2;
    kernel[2][2] = -1;



    // Friendly timing measures that compile with or without -fopenmp
    uint64_t diff;
    struct timespec start, end;
   
    clock_gettime(CLOCK_MONOTONIC, &start);

    // Convolution
    convolution(kernel, paddedMat, mat, dims[0]+2, dims[1]+2);

    clock_gettime(CLOCK_MONOTONIC, &end);

    diff = BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec;
    printf("\nREGULAR: elapsed time = %llu nanoseconds\n\n", (long long unsigned int) diff);


    
    // Flat kernel test
    clock_gettime(CLOCK_MONOTONIC, &start);
    convolutionFlatKernel(paddedMat, mat, dims[0]+2, dims[1]+2);
    clock_gettime(CLOCK_MONOTONIC, &end);
    diff = BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec;
    printf("\nFLAT: elapsed time = %llu nanoseconds\n\n", (long long unsigned int) diff);


    matToImage("processedImage.jpg", mat, dims);

    return 0;
}
