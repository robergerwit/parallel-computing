#ifndef my_header
#define my_header
double* mvp(double** mat, double* vec, int n);
void printMatVec(double** mat, double* vec, double* out, int n);
void freeMat(double** mat, int n);
double** allocMat(int n);
void assignMat(double** mat, int n);
double* allocVec(int n);
void assignVec(double* vec, int n);
#endif
