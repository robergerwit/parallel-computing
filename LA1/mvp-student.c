#include <stdio.h>
#include <stdlib.h>
#include "myheader.h"

double* mvp(double** mat, double* vec, int n){

	double* out = (double*)malloc(n*sizeof(double));

	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			out[i] += mat[i][j] * vec[j];
		}
	}

	return out;
}

void printMatVec(double** mat, double* vec, double* out, int n){
	
	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			printf("%f ", mat[i][j]);

			if(j == n-1 && i == 2)
				printf("\t%f   =   \t%f", vec[i], out[i]);
			else if(j == n-1)
				printf("\t%f\t%f", vec[i], out[i]);
		}
		printf("\n");
	}
}

void freeMat(double** mat, int n){
        
	for(int i = 0; i < n; i++){
                free(mat[i]);
        }
        free(mat);
}

double** allocMat(int n){
        
	double** mat = (double**)malloc(n*sizeof(double*));
        for(int i = 0; i < n; i++){
                mat[i] = (double*)malloc(n*sizeof(double));
        }
	
	return mat;
}

void assignMat(double** mat, int n){
	
        for(int i = 0; i < n; i++)
                for(int j = 0; j < n; j++){
                        if(i == j)
                                mat[i][j] = 2.0;
                        else if(i - j == 1 || i - j == -1)
                                mat[i][j] = 1.0;
                        else
                                mat[i][j] = 0.0;
        }
}

double* allocVec(int n){
	
	double* vec = (double*)malloc(n*sizeof(double));
	return vec;
}

void assignVec(double* vec, int n){
	
        for(int i = 0; i < n; i++){
                vec[i] = 1.0;
        }
}

int main(int argc, char *argv[]){

	// Alloc
	int n = 5;
	double** mat = allocMat(n);
	double* vec = allocVec(n);
	
	// Fill
	assignMat(mat, n);
	assignVec(vec, n);
        
	// MVP
	double* out = mvp(mat, vec, n);

	// Print
	printMatVec(mat, vec, out, n);

	// Free
	freeMat(mat, n);
	free(vec);
	free(out);

    	return 0;
}

 
