#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define GRID_LENGTH 10 

void printGrid(int** grid) {
	
	printf("\n");
        for(int i = 0 ; i < GRID_LENGTH; i++) {
                for(int j = 0; j < GRID_LENGTH; j++) {
                        if(grid[i][j] == 1)
				printf("*");
			else
				printf("-");
				
		}
		printf("\n");
	}
	printf("\n");
}

void timePasses(int** grid) {
	
	// alloc temp grid
        int** newGrid = (int**)malloc(GRID_LENGTH*sizeof(int*));
        for(int i = 0; i < GRID_LENGTH; i++) {
                newGrid[i] = (int*)malloc(GRID_LENGTH*sizeof(int));
        }

        // fill temp grid
        for(int i = 0 ; i < GRID_LENGTH; i++)
                for(int j = 0; j < GRID_LENGTH; j++)
                        newGrid[i][j] = 0;

	// calculate next state
	for(int i = 1; i < GRID_LENGTH-1; i++) {
		for(int j = 1; j < GRID_LENGTH-1; j++) {
			
			// neighborhood
			int count = 0;
			for(int k = -1 ; k <= 1; k++)
				for(int l = -1; l <= 1; l++)
					count += grid[i+k][j+l];
			
			count -= grid[i][j];
			newGrid[i][j] = count;
		}
	}
	
	// update grid based on calculations
	for(int i = 0; i < GRID_LENGTH; i++) {
		for(int j = 0; j < GRID_LENGTH; j++) {
			
			// death
			if(newGrid[i][j] >= 4 || newGrid[i][j] <= 1)
				grid[i][j] = 0;
			// brith
			else if(newGrid[i][j] == 3)
				grid[i][j] = 1;
		}
	}	

	// free temp grid
        for(int i = 0; i < GRID_LENGTH; i++) {
                free(newGrid[i]);
        }
        free(newGrid);
}

int main(int argc, char** argv) {
	
	// alloc
	int** grid = (int**)malloc(GRID_LENGTH*sizeof(int*));
	for(int i = 0; i < GRID_LENGTH; i++) {
		grid[i] = (int*)malloc(GRID_LENGTH*sizeof(int));
	}

	// fill
	for(int i = 0 ; i < GRID_LENGTH; i++)
		for(int j = 0; j < GRID_LENGTH; j++)
			grid[i][j] = 0;

	grid[3][1] = 1;
	grid[3][2] = 1;
	grid[3][3] = 1;

	while(1==1) {
		printGrid(grid);
		getchar();
		timePasses(grid);
	}
	
	// dealloc
        for(int i = 0; i < GRID_LENGTH; i++) {
                free(grid[i]);
        }
	free(grid);

}
