#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

// Allocates a matrix of size N by M
double** allocMatNxM(int n, int m){

        double** mat = (double**)malloc(n*sizeof(double*));
        for(int i = 0; i < n; i++){
                mat[i] = (double*)malloc(m*sizeof(double));
        }

        return mat;
}

// Fills a N by M matrix in the 'B' matrix spec
void fillMatNxM_B(double** mat, int n, int m){

        for(int i = 0; i < n; i++)
                for(int j = 0; j < m; j++) {
			mat[j][i] = i + 3*j;
        }
}

// Fills a N by M matrix in the 'A' matrix spec
void fillMatNxM_A(double** mat, int n, int m){
    
        for(int i = 0; i < n; i++)
                for(int j = 0; j < m; j++) {
                        mat[i][j] = 2*i + j;
        }
}

// Displays contents of an N by M matrix
void printMatNxM(double** mat, int n, int m){

        for(int i = 0; i < n; i++){
                for(int j = 0; j < m; j++){
                        printf("%f ", mat[i][j]);
                }
                printf("\n");
        }
        printf("\n");
}

void freeMat(double** mat, int n){
        
        for(int i = 0; i < n; i++){
                free(mat[i]);
        }
        free(mat);
}

double** transpose(double** mat, int N, int M) {

	double** out = allocMatNxM(M,N);
	for(int i = 0; i < N; i++) {
		for(int j = 0; j < M; j++) {
			out[i][j] = mat[j][i];
		}
	}

	freeMat(mat, N);
	return out;
}

int main (int argc, char *argv[])
{
    int numranks, rank, N=2000, M=2000;
    MPI_Status stat;
    MPI_Request request = MPI_REQUEST_NULL;

    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numranks);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
 
    if(numranks < 2) {
    	printf("Requires at least 1 master node and 1 worker node.\n");
	exit(1);
    }

    // Every rank gets a copy of A
    double** A = allocMatNxM(N,M);
    fillMatNxM_A(A, N, M);

    // Rank specific allocs
    double** rank0_B;
    double* rankN_B;
    double** rank0_C;
    double* rankN_C;

    double start = MPI_Wtime();

    if(rank == 0) {

        rank0_B = allocMatNxM(N,M);
	fillMatNxM_B(rank0_B, N, M);
	rank0_C = allocMatNxM(N,M);
	
	//printf("A:\n");
    	//printMatNxM(A, N, M);
	//printf("B (transposed):\n");
	//printMatNxM(rank0_B, N, M);

	int receiver = 1;
	for(int i = 0; i < N; i++) {
	
		if(receiver == numranks)
			receiver = 1;

		MPI_Isend(&rank0_B[i][0], M, MPI_DOUBLE, receiver, 0, MPI_COMM_WORLD, &request);
		MPI_Irecv(&rank0_C[i][0], M, MPI_DOUBLE, receiver, 0, MPI_COMM_WORLD, &request);

		receiver++;
	}
    }

    else {

	rankN_B = (double*)malloc(M*sizeof(double));
	rankN_C = (double*)malloc(M*sizeof(double));

	for(int i = rank-1; i < N; i+=(numranks-1)) {
	
		MPI_Recv(rankN_B, M, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &stat);

		double result;
		for(int i = 0; i < N; i++) {
			result = 0.0;
			for(int j = 0; j < M; j++) {
				result += A[i][j] * rankN_B[j];
			}
			rankN_C[i] = result;
		}

		MPI_Send(rankN_C, M, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}
    }

    MPI_Barrier(MPI_COMM_WORLD);

    if(rank == 0) {
        
	//printf("Result:\n");

        rank0_C = transpose(rank0_C, N, M);
        //printMatNxM(rank0_C, M, N); 

	printf("Time elapsed: %fs\n", MPI_Wtime() - start);
    	
    }

    MPI_Finalize();
}
